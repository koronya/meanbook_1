# README #

"풀스택 개발자를 위한 MEAN 스택 입문"을 통해서
스터디한 결과물을 올리기 위한 프로젝트!



# 책의 목차

### chapter1. MEAN 스택, 자바스크립트의 거침없는 질주

## Part1. Node.js
### Chapter2. 노드란
### Chapter3. 모듈과 노드 패키지 관리자
### Chapter4. 노드 프로그래밍 모델
### Chapter5. 핵심 모듈
### Chapter6. 노드 서버 구축하기

## Part2. MongoDB
### Chapter7. 몽고디비란
### Chapter8. 몽구스로 몽고디비 쉽게 다루기
### Chapter9. 예제 앱에 몽고디비와 몽구스 접목하기
### Chapter10. SQL, 몽고디비의 대안

## Part3. Express
### Chapter11. 익스프레스란
### Chapter12. 익스프레스 앱 아키텍처
### Chapter13. 예제 앱에 익스프레스 접목하기
### Chapter14. 하피, 익스프레스의 대안

## Part4. AngularJS
### Chapter15. 앵귤러란
### Chapter16. 데이터 바인딩
### Chapter17. 앵귤러 지시자
### Chapter18. 컨트롤러
### Chapter19. 클라이언트 쪽 라우팅
### Chapter20. 예제 앱에 앵귤러 접목하기

## Part5. 개발 환경
### Chapter21. 작업 실행기(gulp)
### Chapter22. 디버깅
### Chapter23. 테스트